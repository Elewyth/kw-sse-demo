package ch.kwsoft.teamday.ssedemo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Controller
@RequestMapping("/sse")
public class SseController {

    private static final Set<SseEmitter> emitters = ConcurrentHashMap.newKeySet();

    @GetMapping("/subscribe")
    public SseEmitter subscribe() {
        SseEmitter emitter = new SseEmitter();

        emitters.add(emitter);

        emitter.onCompletion(() -> emitters.remove(emitter));
        emitter.onTimeout(() -> emitters.remove(emitter));

        return emitter;
    }

    @PostMapping("/publish")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void publishEvent(
            @RequestParam String name,
            @RequestParam String message) {
        ChatEvent event = new ChatEvent(name, message, LocalTime.now());
        new HashSet<>(emitters).forEach(
                emitter -> {
                    try {
                        emitter.send(event);
                    } catch (IOException e) {
                        emitters.remove(emitter);
                    }
                }
        );
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class ChatEvent {
        String name;
        String message;
        LocalTime timestamp;
    }
}
